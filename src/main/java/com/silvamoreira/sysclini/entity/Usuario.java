package com.silvamoreira.sysclini.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


@Entity
@Table(name="usuario", schema="comum")
public class Usuario {
	
	//TODO Verificar se usa AUTO, IDENTITY ou SEQUENCE
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_usuario", updatable = false, nullable = false)
	private int id;
	
	@NotEmpty(message="Este campo não pode ser nulo")
	@Size(min=6, max=20, message="Nome de usuário deve ter entre 6 e 20 caracteres")
	@Column(name="username")
	private String username;
	
	@NotEmpty(message="Este campo não pode ser nulo")
	@Column(name="password")
	private String password;
	
	@NotEmpty(message="Este campo não pode ser nulo")
	@Column(name="email")
	private String email;
	
	@NotEmpty(message="Este campo não pode ser nulo")
	@Column(name="telefone")
	private String telefone;
	
	@Column(name="enabled")
	private boolean enabled;
	
	@ManyToMany(fetch=FetchType.LAZY,
			cascade= {CascadeType.PERSIST, CascadeType.MERGE,
			
	})
	@JoinTable(name="usuario_papeis", schema="comum",
	joinColumns=@JoinColumn(name="id_usuario"),
	inverseJoinColumns=@JoinColumn(name="id_papel")) 
	private List<Papel> papeis;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_empresa")
	private Empresa empresa;
	
	@Transient
	private String senhaRepetida;
	
	public Usuario() {
		
	}

	public Usuario(String username, String password, List<Papel> papeis) {
		super();
		this.username = username;
		this.password = password;
		this.papeis = papeis;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public List<Papel> getPapeis() {
		return papeis;
	}

	public void setPapeis(List<Papel> papeis) {
		this.papeis = papeis;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getSenhaRepetida() {
		return senhaRepetida;
	}

	public void setSenhaRepetida(String senhaRepetida) {
		this.senhaRepetida = senhaRepetida;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", username=" + username + ", password=" + password + ", email=" + email
				+ ", telefone=" + telefone + ", enabled=" + enabled + ", papeis=" + papeis + ", empresa=" + empresa
				+ ", senhaRepetida=" + senhaRepetida + "]";
	}

}
