package com.silvamoreira.utils;


public class ArquivoUtils {

public static String getTipoArquivo(String arquivo) {
		
		if (arquivo == null)
			return null;
		
		int indexPonto = arquivo.lastIndexOf(".");
		
		if (indexPonto > 0)
			return arquivo.substring(indexPonto+1, arquivo.length());
		else
			return arquivo;
		
	}
}
