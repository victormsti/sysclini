package com.silvamoreira.sysclini.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.silvamoreira.sysclini.entity.Usuario;


public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

}