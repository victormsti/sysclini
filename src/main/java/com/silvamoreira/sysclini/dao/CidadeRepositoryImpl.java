package com.silvamoreira.sysclini.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.util.CollectionUtils;

import com.silvamoreira.sysclini.entity.Cidade;

public class CidadeRepositoryImpl implements CidadeRepositoryCustom{

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Cidade> findByEstadoId(int estadoId) {

		List<Cidade> cidades;
		cidades = em.createQuery("SELECT c FROM Cidade c WHERE c.estado.id = :estadoId")
				.setParameter("estadoId", estadoId).getResultList();
		
		return CollectionUtils.isEmpty(cidades ) ? null : cidades;
	}
}
