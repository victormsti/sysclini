package com.silvamoreira.sysclini.service;

import java.util.List;

import com.silvamoreira.sysclini.entity.Empresa;
import com.silvamoreira.sysclini.entity.Usuario;


public interface EmpresaService {
	
	public Empresa findById(int id);
	
	public List<Empresa> findAll();
	
	public Empresa findByUserId(int userId);
	
	public void save (Empresa empresa);

}
