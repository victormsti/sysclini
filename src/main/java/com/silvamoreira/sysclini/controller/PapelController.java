package com.silvamoreira.sysclini.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.silvamoreira.sysclini.entity.Papel;
import com.silvamoreira.sysclini.service.PapelService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class PapelController {

	@Autowired
	private PapelService papelService;
	
	@GetMapping("/papeis/byUsername")
	public List<Papel> getPapeisByUsername(@RequestParam("username") String username) {

		return papelService.findByUsername(username);
	}
	
	@GetMapping("/papeis")
	public List<Papel> getAllPapeis() {

		return papelService.findAll();
	}
}
