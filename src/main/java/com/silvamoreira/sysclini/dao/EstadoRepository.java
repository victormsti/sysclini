package com.silvamoreira.sysclini.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.silvamoreira.sysclini.entity.Estado;


public interface EstadoRepository extends JpaRepository<Estado, Integer> {

	
}
