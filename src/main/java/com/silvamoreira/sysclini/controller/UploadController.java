package com.silvamoreira.sysclini.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.silvamoreira.sysclini.entity.Paciente;
import com.silvamoreira.sysclini.service.PacienteService;
import com.silvamoreira.sysclini.service.StorageService;
import com.silvamoreira.utils.ArquivoUtils;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UploadController {
 
  @Autowired
  StorageService storageService;
  
  @Autowired
  ResourceLoader resourceLoader;
  
  @Autowired
  PacienteService pacienteService;
 
  List<String> files = new ArrayList<String>();
  
  @PostMapping("/postFile")
  public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file, 
		  @RequestParam(name="idPessoa",required=false) String idPessoa,
		  @RequestParam(name="idPaciente",required=false) String idPaciente) {
    Paciente paciente;
	  String message = "";
    String id = idPessoa != null ? idPessoa : null;
    String extensao = ArquivoUtils.getTipoArquivo(file.getOriginalFilename());
    try {
      storageService.store(file, ( id != null ? (id+ "." + extensao) : file.getOriginalFilename()));
      files.add(( id != null ? (id+ "." + extensao) : file.getOriginalFilename()));
 
      message = "You successfully uploaded " + ( id != null ? (id+ "." + extensao) : file.getOriginalFilename())
    		  + "!";
      
      paciente = pacienteService.findById(Integer.parseInt(idPaciente));
      
      paciente.getPessoa().setNomeArquivo(id + "." + extensao);
      pacienteService.save(paciente);
      
      return ResponseEntity.status(HttpStatus.OK).body(message);
    } catch (Exception e) {
      message = "FAIL to upload " + ( id != null ? (id+ "." + extensao) : file.getOriginalFilename()) + "!";
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
    }
  }
 
  @GetMapping("/getallfiles")
  public ResponseEntity<List<String>> getListFiles(Model model) {
    List<String> fileNames = files
        .stream().map(fileName -> MvcUriComponentsBuilder
            .fromMethodName(UploadController.class, "getFile", fileName).build().toString())
        .collect(Collectors.toList());
 
    return ResponseEntity.ok().body(fileNames);
  }
 
//  @GetMapping("/files")
//  public String getFile(@RequestParam(name = "nomeArquivo", required = false) String nomeArquivo ) throws IOException {
//   if(nomeArquivo != null) {
//	  Resource file = storageService.loadFile(nomeArquivo);
//    return file.getURL().getFile();
//   }
//   return null;
//  }
  
  @GetMapping("/files")
  @ResponseBody
  public ResponseEntity<Resource> getFile(@RequestParam(name = "nomeArquivo", required = false) String nomeArquivo ) throws IOException {
   if(nomeArquivo != null) {
	  Resource file = storageService.loadFile(nomeArquivo);
	  return ResponseEntity.ok()
		        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
		        .body(file);
		  
   }
   return null;
  }
  
//  @GetMapping("/files/{filename:.+}")
//  @ResponseBody
//  public ResponseEntity<Resource> getSpecificFile(@PathVariable String filename) throws MalformedURLException {
//    Resource file = storageService.loadFile(filename);
//    return ResponseEntity.ok()
//        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
//        .body(file);
//  }
  
//  @RequestMapping(value = "/files/{filename:.+}", method = RequestMethod.GET)
//  public ResponseEntity<Resource> getImage(@PathVariable String filename) {
//
//      try {
//    	  Resource file = storageService.loadFile(filename);
//          //String path = Paths.get(ROOT, filename).toString();
//          Resource loader = resourceLoader.getResource("file:" + file.getURL().getPath());
//          return new ResponseEntity<Resource>(loader, HttpStatus.OK);
//      } catch (Exception e) {
//          return new ResponseEntity<Resource>(HttpStatus.NOT_FOUND);
//      }
//  }
}