package com.silvamoreira.sysclini.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.silvamoreira.sysclini.entity.Etnia;


public interface EtniaRepository extends JpaRepository<Etnia, Integer> {

	
}
