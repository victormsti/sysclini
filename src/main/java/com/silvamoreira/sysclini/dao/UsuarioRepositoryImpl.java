package com.silvamoreira.sysclini.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.util.CollectionUtils;

import com.silvamoreira.sysclini.entity.Usuario;

public class UsuarioRepositoryImpl implements UsuarioRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public Usuario findByUsername(String username) {

		List<Usuario> usuarios;
		usuarios = em.createQuery("SELECT u FROM Usuario u WHERE u.username LIKE :username")
				.setParameter("username", username).getResultList();
		
		return CollectionUtils.isEmpty(usuarios ) ? null : usuarios.get(0);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Usuario findByEmail(String email) {

		List<Usuario> usuarios;
		usuarios = em.createQuery("SELECT u FROM Usuario u WHERE u.email LIKE :email")
				.setParameter("email", email).getResultList();
		
		return CollectionUtils.isEmpty(usuarios ) ? null : usuarios.get(0);
	}
}