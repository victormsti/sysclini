package com.silvamoreira.sysclini.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.silvamoreira.sysclini.entity.Paciente;
import com.silvamoreira.sysclini.entity.ResultadoAvaliacaoInicial;
import com.silvamoreira.sysclini.service.PacienteService;
import com.silvamoreira.sysclini.service.ResultadoAvaliacaoInicialServiceImpl;
import com.silvamoreira.utils.OperacaoUtils;

@Controller
public class ResultadoAvaliacaoInicialController extends GenericController{

	@Autowired
	PacienteService pacienteService;
	
	@Autowired
	ResultadoAvaliacaoInicialServiceImpl resultadoAvaliacaoInicialService;
	
	ResultadoAvaliacaoInicial resultTest;
	
	@ModelAttribute
	public void addAttributes(Model model) {
		ResultadoAvaliacaoInicial resultadoAvaliacaoInicial = new ResultadoAvaliacaoInicial();
	}
	
	@GetMapping("/admin_empresa/resultado_avaliacao/pacientes")
	public String paginaPacientes(Model model) {

		init();
		List<Paciente> pacientes = pacienteService.findAll();
		model.addAttribute("pacientes", pacientes);
		
		model.addAttribute("tituloOperacao", OperacaoUtils.TITULO_GERENCIAR_RESULTADO_AVALIACAO_PACIENTE);
		model.addAttribute("descricaoOperacao", OperacaoUtils.DESCRICAO_GERENCIAR_RESULTADO_AVALIACAO_PACIENTE);

		return "/admin_empresa/listar_pacientes_resultado_avaliacao_inicial";
	}
	
	@GetMapping("/admin_empresa/resultado_avaliacao/prepararAlterarResultadoAvaliacaoPaciente")
	public String prepararAlterarResultadoAvaliacaoPaciente(@RequestParam("pacienteId") int pacienteId, Model model) {
		
		ResultadoAvaliacaoInicial resultadoAvaliacaoInicial = new ResultadoAvaliacaoInicial();
		Paciente paciente = pacienteService.findById(pacienteId);
		
		if(resultTest != null)
			resultadoAvaliacaoInicial = resultTest;
		
//		if(paciente != null) {
//			
//		}
		
		model.addAttribute("resultadoAvaliacaoInicial", resultadoAvaliacaoInicial);
		model.addAttribute("paciente", paciente);

		model.addAttribute("tituloOperacao", OperacaoUtils.TITULO_CADASTRAR_RESULTADO_AVALIACAO_PACIENTE);
		model.addAttribute("descricaoOperacao", OperacaoUtils.DESCRICAO_CADASTRAR_RESULTADO_AVALIACAO_PACIENTE);
		
		return "/admin_empresa/cadastrar_alterar_resultado_avaliacao_inicial";
	}
	
	@RequestMapping(value="/admin_empresa/resultado_avaliacao/cadastrarAlterarResultadoAvaliacao",  method = RequestMethod.POST)
	public String cadastrarAlterarResultadoAvaliacao(@ModelAttribute("resultadoAvaliacaoInicial") @Valid ResultadoAvaliacaoInicial resultadoAvaliacaoInicial, Model model, BindingResult bindingResult) {
		resultTest = resultadoAvaliacaoInicial;
		return "home";
	}
}
