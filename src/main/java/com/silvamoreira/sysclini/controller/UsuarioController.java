package com.silvamoreira.sysclini.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.silvamoreira.sysclini.entity.Empresa;
import com.silvamoreira.sysclini.entity.Papel;
import com.silvamoreira.sysclini.entity.Usuario;
import com.silvamoreira.sysclini.service.EmpresaService;
import com.silvamoreira.sysclini.service.PapelService;
import com.silvamoreira.sysclini.service.UsuarioService;
import com.silvamoreira.utils.OperacaoUtils;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UsuarioController extends GenericController {

	private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	private String nomeUsuario;
	private int id;
	private UsuarioService usuarioService;

	@Autowired
	private PapelService papelService;

	@Autowired
	private EmpresaService empresaService;

	@Autowired
	public UsuarioController(UsuarioService usuarioService, EmpresaService empresaService) {
		this.usuarioService = usuarioService;
	}

	// @Autowired
	// PasswordEncoder passwordEncoder;

	// método chamado antes dos request mappings
	@ModelAttribute
	public void addAttributes(Model model) {

		model.addAttribute("papeis", papelService.findAll());
		model.addAttribute("usuario", new Usuario());
		model.addAttribute("empresa", new Empresa());
	}

	@GetMapping("/admin_sistema/usuarios")
	public List<Usuario> getUsuarios() {
		return usuarioService.findAll();

	}

	@GetMapping("/admin_sistema/usuario")
	public Usuario getUsuario(@RequestParam(name = "id", required = false) String id) {
		if (id != null) {
			return usuarioService.findById(Integer.parseInt(id));
		} else
			return null;
	}

	@GetMapping("/usuarios/getUserByUsername")
	public Usuario getUsuarioByUsername(@RequestParam("username") String username) {

		return usuarioService.findByUsername(username);
	}

	@GetMapping("/usuarios/getUserByEmail")
	public Usuario getUsuarioByEmail(@RequestParam("email") String email) {

		return usuarioService.findByEmail(email);
	}

	@PostMapping("/admin_sistema/usuarios")
	public Usuario createUpdate(@RequestBody Usuario usuario) {
		List<Papel> papeis = new ArrayList<Papel>();

		for (Papel papel : usuario.getPapeis()) {
			papel = papelService.findById(papel.getId());
			papeis.add(papel);
		}
		usuario.setPapeis(papeis);
		usuario.setEmpresa(empresaService.findById(usuario.getEmpresa().getId()));
		usuario.setPassword(passwordEncoder.encode(usuario.getPassword()));
		usuarioService.save(usuario);
		return usuario;
	}

	@GetMapping("/admin/usuarios")
	public String paginaUsuarios(Model model) {

		init();

		List<Usuario> usuarios = usuarioService.findAll();
		model.addAttribute("usuarios", usuarios);

		model.addAttribute("tituloOperacao", OperacaoUtils.TITULO_GERENCIAR_USUARIO);
		model.addAttribute("descricaoOperacao", OperacaoUtils.DESCRICAO_GERENCIAR_USUARIO);

		return "listar_usuarios";
	}

	@GetMapping("/admin/usuarios/prepararCadastrarUsuario")
	public String prepararCadastrarUsuario(Model model, @ModelAttribute("usuario") Usuario usuario) {

		init();
		operacao = operacaoCadastro;

		usuario.setEmpresa(new Empresa());
		model.addAttribute("usuario", usuario);
		model.addAttribute("empresas", empresaService.findAll());
		model.addAttribute("tituloOperacao", OperacaoUtils.TITULO_CADASTRAR_USUARIO);
		model.addAttribute("descricaoOperacao", OperacaoUtils.DESCRICAO_CADASTRAR_USUARIO);
		model.addAttribute("papeis", papelService.findAll());

		model.addAttribute("operacaoButton", "Cadastrar");
		model.addAttribute("idEmpresaSelecionada", 0);

		return "cadastrar_usuario";
	}

	@GetMapping("/admin/usuarios/excluir")
	public String excluirUsuario(@RequestParam("usuarioId") int usuarioId, Model theModel) {

		usuarioService.deleteById(usuarioId);

		return "redirect:/admin/usuarios";
	}

	@SuppressWarnings("unused")
	@GetMapping("/admin/usuarios/prepararAlterarUsuario")
	public String prepararAlterarUsuario(@RequestParam("usuarioId") int usuarioId, Model model,
			@ModelAttribute("papeis") List<Papel> papeis, final BindingResult bindingResult) {

		init();
		operacao = operacaoAlteracao;
		Usuario usuario = usuarioService.findById(usuarioId);
		usuario.getPapeis();
		id = usuario.getId();
		nomeUsuario = usuario.getUsername();
		List<Papel> papeisUsuario = papelService.findByUserId(usuarioId);
		List<Empresa> empresas = empresaService.findAll();

		if (usuario != null) {
			model.addAttribute("usuario", usuario);

			model.addAttribute("empresas", empresas);
			model.addAttribute("tituloOperacao", OperacaoUtils.TITULO_ALTERAR_USUARIO);
			model.addAttribute("descricaoOperacao", OperacaoUtils.DESCRICAO_ALTERAR_USUARIO);

			model.addAttribute("operacaoButton", "Alterar");
			return "alterar_usuario";
		} else {
			adicionarMensagemErro("Usuário não encontrado");
			model.addAttribute("errorMessages", errorList);
			return "home";
		}
	}

	@PostMapping("/admin/usuarios/cadastrarAlterarUsuario")
	public String cadastrarAlterarUsuario(@ModelAttribute("usuario") @Valid Usuario usuario,
			final BindingResult bindingResult, final RedirectAttributes redirectAttributes,
			@ModelAttribute("empresa") Empresa empresa, @ModelAttribute("papeis") List<Papel> papeis, Model model,
			@RequestParam(value = "empresaId", required = false) String empresaId,
			@RequestParam(value = "papelId", required = false) List<String> papeisId) {

		init();

		validar(usuario, papeisId, empresaId);

		// TODO mudar lógica depois
		if ((hasErrors() || bindingResult.hasErrors())) {
			model.addAttribute("errorMessages", errorList);

			if (operacao.equalsIgnoreCase(operacaoAlteracao)) {
				model.addAttribute("tituloOperacao", OperacaoUtils.TITULO_ALTERAR_USUARIO);
				model.addAttribute("descricaoOperacao", OperacaoUtils.DESCRICAO_ALTERAR_USUARIO);

				// necessário para não dar erro no redirecionamento
				// redirectAttributes.addFlashAttribute("errors", bindingResult);
				// model.addAttribute("usuario", usuarioService.findById(id));
				model.addAttribute("empresas", empresaService.findAll());
				model.addAttribute("operacaoButton", "Alterar");

				return "alterar_usuario";
			} else {
				model.addAttribute("tituloOperacao", OperacaoUtils.TITULO_CADASTRAR_USUARIO);
				model.addAttribute("descricaoOperacao", OperacaoUtils.DESCRICAO_CADASTRAR_USUARIO);

				// necessário para não dar erro no redirecionamento
				model.addAttribute("usuario", usuario);
				model.addAttribute("empresas", empresaService.findAll());

				// model.addAttribute("papeis", papelService.findAll());

				model.addAttribute("operacaoButton", "Cadastrar");
				model.addAttribute("idEmpresa", Integer.parseInt(empresaId));

				return "cadastrar_usuario";
			}
		} else {

			// Complementar cadastro
			String encodedPassword = passwordEncoder.encode(usuario.getPassword());

			encodedPassword = "{bcrypt}" + encodedPassword;

			usuario.setPassword(encodedPassword);
			usuario.setEnabled(true);

			if (operacao.equalsIgnoreCase(operacaoAlteracao)) {
				usuario.setId(id);
			}

			usuarioService.save(usuario);

			if (operacao.equalsIgnoreCase(operacaoCadastro))
				adicionarMensagemSucesso("Usuário cadastrado com sucesso!");
			else
				adicionarMensagemSucesso("Usuário alterado com sucesso!");

			model.addAttribute("successMessages", successList);
			return "home";
		}
	}

	public void validar(Usuario usuario, List<String> papeisId, String empresaId) {
		List<Papel> papeisSelecionados = new ArrayList<Papel>();
		Papel papel = new Papel();

		if (papeisId != null) {
			for (String id : papeisId) {
				papel = papelService.findById(Integer.parseInt(id));
				papeisSelecionados.add(papel);
			}

			if (!papeisSelecionados.isEmpty()) {
				usuario.setPapeis(papeisSelecionados);
			}
		}

		else
			adicionarMensagemErro("Selecione pelo menos um papel");

		if (usuario.getPassword().isEmpty() && usuario.getSenhaRepetida().isEmpty()) {
			adicionarMensagemErro("Digite as senhas");
		}
		if (!usuario.getPassword().equals(usuario.getSenhaRepetida())) {
			adicionarMensagemErro("As senhas não conferem");
		}

		if (operacao.equalsIgnoreCase(operacaoCadastro)) {
			if (usuario.getUsername() != null && !usuario.getUsername().isEmpty()) {
				Usuario usuarioBd = usuarioService.findByUsername(usuario.getUsername());
				if (usuarioBd != null) {
					if (nomeUsuario != null && nomeUsuario.equalsIgnoreCase(usuario.getUsername())) {
						adicionarMensagemErro("Usuário já cadastrado. Por favor, insira um login diferente");
					}
				}
			}
		} else if (operacao.equalsIgnoreCase(operacaoAlteracao)) {
			Usuario usuarioBd = usuarioService.findById(id);
			if (usuarioBd != null) {
				if (nomeUsuario != null && !nomeUsuario.equalsIgnoreCase(usuarioBd.getUsername())) {
					adicionarMensagemErro("Usuário já cadastrado. Por favor, insira um login diferente");
				}
			}
		}

		if (Integer.parseInt(empresaId) == 0) {
			adicionarMensagemErro("Selecione uma empresa");
		} else {
			Empresa empresaBd = empresaService.findById(Integer.parseInt(empresaId));
			usuario.setEmpresa(empresaBd);
		}
	}
}
