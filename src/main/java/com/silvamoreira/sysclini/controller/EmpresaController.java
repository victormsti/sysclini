package com.silvamoreira.sysclini.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.silvamoreira.sysclini.entity.Cidade;
import com.silvamoreira.sysclini.entity.Empresa;
import com.silvamoreira.sysclini.entity.Endereco;
import com.silvamoreira.sysclini.entity.Estado;
import com.silvamoreira.sysclini.entity.Papel;
import com.silvamoreira.sysclini.entity.Usuario;
import com.silvamoreira.sysclini.service.CidadeService;
import com.silvamoreira.sysclini.service.EmpresaService;
import com.silvamoreira.sysclini.service.EstadoService;
import com.silvamoreira.utils.OperacaoUtils;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class EmpresaController extends GenericController {

	@Autowired
	private EmpresaService empresaService;

	@Autowired
	private EstadoService estadoService;

	@Autowired
	private CidadeService cidadeService;
	
	private int id;

	@ModelAttribute
	public void addAttributes(Model model) {

		Empresa empresa = new Empresa();
		empresa.setEndereco(new Endereco());
		model.addAttribute("empresa", empresa);
		model.addAttribute("cidadeSelecionada", new Cidade());
		model.addAttribute("estados", estadoService.findAll());
	}
	
	@GetMapping("/empresas")
	public List<Empresa> getAllEmpresas() {

		return empresaService.findAll();
	}
	
	@GetMapping("/admin_sistema/empresa")
	public Empresa getEmpresaById(@RequestParam(name = "id", required = false) String id) {

		if(id != null) {
			return empresaService.findById(Integer.parseInt(id));	
		}
		else return null;
		
	}
	
	@PostMapping("/admin_sistema/empresas")
	public Empresa createUpdate(@RequestBody Empresa empresa) {
		empresa.getEndereco().setCidade(cidadeService.findById(empresa.getEndereco().getCidade().getId()));
		empresaService.save(empresa);
		return empresa;
	}

	@GetMapping("/admin/empresas")
	public String paginaUsuarios(Model model) {

		init();
		int idEstado = 0;
		List<Empresa> empresas = empresaService.findAll();
		model.addAttribute("empresas", empresas);
		model.addAttribute("idEstado", idEstado);

		model.addAttribute("tituloOperacao", OperacaoUtils.TITULO_GERENCIAR_EMPRESA);
		model.addAttribute("descricaoOperacao", OperacaoUtils.DESCRICAO_GERENCIAR_EMPRESA);

		return "/admin/listar_empresas";
	}

	@GetMapping("/admin/empresas/prepararCadastrarEmpresa")
	public String prepararCadastrarEmpresa(Model model) {

		init();
		setOperacao(operacaoCadastro);

		model.addAttribute("tituloOperacao", OperacaoUtils.TITULO_CADASTRAR_EMPRESA);
		model.addAttribute("descricaoOperacao", OperacaoUtils.DESCRICAO_CADASTRAR_EMPRESA);

		model.addAttribute("operacaoButton", "Cadastrar");

		return "admin/cadastrar_empresa";
	}

	@RequestMapping(value = "/admin/empresas/getCidades", headers = "Accept=*/*", method = RequestMethod.GET)
	public @ResponseBody List<Cidade> getCidades(@RequestParam("estadoId") String estadoId) {

		Estado estado = null;

		if (Integer.parseInt(estadoId) != 0)
			estado = estadoService.findById(Integer.parseInt(estadoId));

		if (estado != null) {
			return cidadeService.findByEstadoId(estado.getId());
		} else
			return new ArrayList<Cidade>();
	}
	
	@SuppressWarnings("unused")
	@GetMapping("/admin/empresas/prepararAlterarEmpresa")
	public String prepararAlterarEmpresa(@RequestParam("empresaId") int empresaId, Model model) {

		init();
		
		operacao = operacaoAlteracao;
		Empresa empresa = empresaService.findById(empresaId);
		empresa.getEndereco();
//		empresa.getEndereco().getCidade();
//		empresa.getEndereco().getCidade().getEstado();
		id = empresaId;

		if (empresa != null) {
			model.addAttribute("empresa", empresa);
			initLists(model, empresa);
			
			model.addAttribute("tituloOperacao", OperacaoUtils.TITULO_ALTERAR_EMPRESA);
			model.addAttribute("descricaoOperacao", OperacaoUtils.DESCRICAO_ALTERAR_EMPRESA);

			model.addAttribute("operacaoButton", "Alterar");
			return "/admin/alterar_empresa";
		} else {
			adicionarMensagemErro("Empresa não encontrada");
			model.addAttribute("errorMessages", errorList);
			return "home";
		}
	}
		
	@PostMapping("/admin/empresas/cadastrarAlterarEmpresa")
	public String cadastrarAlterarUsuario(@ModelAttribute("empresa") @Valid Empresa empresa,
			final BindingResult bindingResult, final RedirectAttributes redirectAttributes, Model model,
			@RequestParam(value = "cidadeId", required = true) String cidadeId) {

		init();
		
		validar(empresa, cidadeId);

		if ((hasErrors() || bindingResult.hasErrors())) {
			model.addAttribute("errorMessages", errorList);

			if (operacao.equalsIgnoreCase(operacaoAlteracao)) {
				
				model.addAttribute("tituloOperacao", OperacaoUtils.TITULO_ALTERAR_EMPRESA);
				model.addAttribute("descricaoOperacao", OperacaoUtils.DESCRICAO_ALTERAR_EMPRESA);

				// necessário para não dar erro no redirecionamento
				model.addAttribute("empresa", empresa);

				model.addAttribute("operacaoButton", "Alterar");

				return "admin/alterar_empresa";
			}
			
				model.addAttribute("tituloOperacao", OperacaoUtils.TITULO_CADASTRAR_EMPRESA);
				model.addAttribute("descricaoOperacao", OperacaoUtils.DESCRICAO_CADASTRAR_EMPRESA);

				// necessário para não dar erro no redirecionamento
				model.addAttribute("empresa", empresa);

				model.addAttribute("operacaoButton", "Cadastrar");

				return "admin/cadastrar_empresa";
		}

		else {

			if (operacao.equalsIgnoreCase(operacaoAlteracao))
				empresa.setId(id);

			empresaService.save(empresa);

			if (operacao.equalsIgnoreCase(operacaoCadastro))
				adicionarMensagemSucesso("Empresa cadastrada com sucesso!");
			else
				adicionarMensagemSucesso("Empresa alterada com sucesso!");

			model.addAttribute("successMessages", successList);
			return "home";
		}
	}

	public void validar(Empresa empresa, String cidadeId) {

		if (Integer.parseInt(cidadeId) == 0) {
			adicionarMensagemErro("Selecione o Estado e a Cidade");
		}
		
		else {
			empresa.getEndereco().setCidade(cidadeService.findById(Integer.parseInt(cidadeId)));
		}
		
		if(empresa.getDescricao() == null || empresa.getDescricao().isEmpty()) {
			adicionarMensagemErro("Descrição da Empresa é um campo obrigatório");
		}
		
	}
	
	private void initLists(Model model, Empresa empresa) {
		 List<Cidade> cidades = cidadeService.findByEstadoId(empresa.getEndereco().getCidade().getEstado().getId());
        model.addAttribute("cidades", cidades);
    }
}
