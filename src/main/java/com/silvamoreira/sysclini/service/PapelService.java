package com.silvamoreira.sysclini.service;

import java.util.List;

import com.silvamoreira.sysclini.entity.Papel;

public interface PapelService {
	
	public Papel findById(int id);
	
	public List<Papel> findAll();
	
	public List<Papel> findByUserId(int userId);

	List<Papel> findByUsername(String username);	
}
