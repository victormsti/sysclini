package com.silvamoreira.sysclini.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.silvamoreira.sysclini.entity.Estado;
import com.silvamoreira.sysclini.service.CidadeService;
import com.silvamoreira.sysclini.service.EstadoService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class EstadoController {

	@Autowired
	private EstadoService estadoService;

	@Autowired
	private CidadeService cidadeService;
	
	@GetMapping("/estados")
	public List<Estado> getUsuarios() {
		return estadoService.findAll();

	}
}
