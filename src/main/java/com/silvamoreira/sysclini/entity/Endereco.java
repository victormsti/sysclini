package com.silvamoreira.sysclini.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="endereco", schema="comum")
public class Endereco {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_endereco", updatable = false, nullable = false)
	private int id;
	
	@Column(name="cep")
	private String cep;
	
	@Column(name="logadouro")
	private String logadouro;
	
	@Column(name="numero")
	private String numero;
	
	@Column(name="bairroPovoado")
	private String bairroPovoado;
	
	@Column(name="complemento")
	private String complemento;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_cidade")
	private Cidade cidade;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLogadouro() {
		return logadouro;
	}

	public void setLogadouro(String logadouro) {
		this.logadouro = logadouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairroPovoado() {
		return bairroPovoado;
	}

	public void setBairroPovoado(String bairroPovoado) {
		this.bairroPovoado = bairroPovoado;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	@Override
	public String toString() {
		return "Endereco [id=" + id + ", cep=" + cep + ", logadouro=" + logadouro + ", numero=" + numero
				+ ", bairroPovoado=" + bairroPovoado + ", complemento=" + complemento + ", cidade=" + cidade + "]";
	}
	
}
