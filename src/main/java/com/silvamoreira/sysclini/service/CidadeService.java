package com.silvamoreira.sysclini.service;

import java.util.List;

import com.silvamoreira.sysclini.entity.Cidade;

public interface CidadeService {
	
	public Cidade findById(int id);
	
	public List<Cidade> findAll();
	
	public List<Cidade> findByEstadoId(int estadoId);
}
