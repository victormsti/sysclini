package com.silvamoreira.sysclini.service;

import java.util.List;

import com.silvamoreira.sysclini.entity.Paciente;


public interface PacienteService {
	
	public Paciente findById(int id);
	
	public List<Paciente> findAll();
	
	public void save (Paciente paciente);

}
