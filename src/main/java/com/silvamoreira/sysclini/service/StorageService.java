package com.silvamoreira.sysclini.service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import com.silvamoreira.sysclini.entity.Foto;
import com.silvamoreira.sysclini.entity.Paciente;
import com.silvamoreira.utils.ArquivoUtils;
@Service
public class StorageService {
 
  Logger log = LoggerFactory.getLogger(this.getClass().getName());
  private final Path rootLocation = Paths.get("src/main/webapp/upload");
 
  public void store(MultipartFile file, String id) {
    try {
      Files.copy(file.getInputStream(), this.rootLocation.resolve(
    		  id != null ? id : file.getOriginalFilename()));
    
    } catch (Exception e) {
      throw new RuntimeException("FAIL!");
    }
  }
 
  public Resource loadFile(String filename) throws MalformedURLException {
    try {
      Path file = rootLocation.resolve(filename);
      Resource resource = new UrlResource(file.toUri());
      if (resource.exists() || resource.isReadable()) {
        return resource;
      } else {
        throw new RuntimeException("FAIL!");
      }
    } catch (MalformedURLException e) {
      throw new RuntimeException("FAIL!");
    }
  }
 
	private void doUpload(Paciente paciente, Foto foto) {

		File uploadRootDir = new File(rootLocation.toString());
		// Create directory if it not exists.
		if (!uploadRootDir.exists()) {
			uploadRootDir.mkdirs();
		}
		MultipartFile[] fileDatas = foto.getFileDatas();
		List<File> uploadedFiles = new ArrayList<File>();
		List<String> failedFiles = new ArrayList<String>();

		for (MultipartFile fileData : fileDatas) {

			String extensao = ArquivoUtils.getTipoArquivo(fileData.getOriginalFilename());
			// Client File Name
			String name = paciente.getPessoa().getId() + "." + extensao.toLowerCase();

			if (name != null && name.length() > 0) {
				try {
					// Create the file at server
					File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + name);

					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
					// Salva em disco
					stream.write(fileData.getBytes());
					stream.close();
					//
					uploadedFiles.add(serverFile);
					paciente.getPessoa().setExtensaoArquivo(extensao);
				} catch (Exception e) {
					System.out.println("Error Write file: " + name.toLowerCase());
					failedFiles.add(name);
				}
			}
		}
	}
  
  public void deleteAll() {
    FileSystemUtils.deleteRecursively(rootLocation.toFile());
  }
 
  public void init() {
    try {
      Files.createDirectory(rootLocation);
    } catch (IOException e) {
      throw new RuntimeException("Could not initialize storage!" + " ==> " + e.getMessage());
    }
  }
}