package com.silvamoreira.sysclini.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.silvamoreira.sysclini.entity.Cidade;
import com.silvamoreira.sysclini.entity.Endereco;
import com.silvamoreira.sysclini.entity.Estado;
import com.silvamoreira.sysclini.entity.Foto;
import com.silvamoreira.sysclini.entity.Paciente;
import com.silvamoreira.sysclini.entity.Pessoa;
import com.silvamoreira.sysclini.entity.Responsavel;
import com.silvamoreira.sysclini.service.CidadeService;
import com.silvamoreira.sysclini.service.EstadoService;
import com.silvamoreira.sysclini.service.PacienteService;
import com.silvamoreira.utils.ArquivoUtils;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class PacienteController extends GenericController {

	@Autowired
	PacienteService pacienteService;

	@Autowired
	private EstadoService estadoService;

	@Autowired
	private CidadeService cidadeService;

	// Guarda os dados do paciente do primeiro passo
	Pessoa pacienteDadosGerais;

	// Guarda os dados do paciente do segundo passo
	Endereco pacienteEndereco;

	// Guarda os dados do responsavel 1
	Responsavel dadosResponsavel1;

	// Guarda os dados do responsavel 2
	Responsavel dadosResponsavel2;

	// Guarda os dados do paciente do terceiro passo
	List<Responsavel> responsaveis = new ArrayList<Responsavel>();

	// Guarda o Estado selecionado
	Estado estadoSelecionado = new Estado();

	// Guarda a Cidade selecionada
	Cidade cidadeSelecionada = new Cidade();

	int idEstadoSelecionado = 0;
	int idCidadeSelecionada = 0;

	// Guarda os dados do paciente do quarto passo
	String foto;

	@GetMapping("/admin_empresa/pacientes")
	public List<Paciente> getPacientes() {

		return pacienteService.findAll();
	}
	
	@GetMapping("/admin_empresa/paciente")
	public Paciente getPacienteById(@RequestParam(name = "id", required = false) String id ) {

		return pacienteService.findById(Integer.parseInt(id));
	}

	@GetMapping("/admin_empresa/getPacientes")
	public List<Paciente> getPacientesAdmin(Model model) {

		return pacienteService.findAll();
	}

	@PostMapping("/admin_empresa/pacientes")
	public Paciente createUpdate(@RequestBody Paciente paciente) {
		// List<Responsavel> responsaveis = new ArrayList<Responsavel>();

		paciente.getPessoa().getEndereco()
				.setCidade(cidadeService.findById(paciente.getPessoa().getEndereco().getCidade().getId()));

	
		pacienteService.save(paciente);
		return paciente;
	}

//	// Necessário para não ficar com valores nulos
//	public void inicializarResponsavel(Responsavel responsavel) {
//		responsavel.getPessoa().setDataNascimento(new Date());
//		responsavel.getPessoa().setNacionalidade("");
//		responsavel.getPessoa().setNumeroIdentidade("");
//		responsavel.getPessoa().setUfIdentidade("");
//		responsavel.getPessoa().setExtensaoArquivo("");
//		responsavel.getPessoa().setNomeArquivo("");
//	}
//
//	// Necessário para não ficar com valores nulos
//	public void inicializarPaciente(Paciente paciente) {
//		paciente.getPessoa().setExtensaoArquivo("");
//	}

}
