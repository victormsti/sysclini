package com.silvamoreira.sysclini.dao;

import java.util.List;

import com.silvamoreira.sysclini.entity.Papel;

public interface PapelRepositoryCustom {

	List<Papel> findByUserId(int userId);

	List<Papel> findByUserUsername(String username);
}
