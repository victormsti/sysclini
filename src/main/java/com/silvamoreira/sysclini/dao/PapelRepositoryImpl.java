package com.silvamoreira.sysclini.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.util.CollectionUtils;

import com.silvamoreira.sysclini.entity.Papel;

public class PapelRepositoryImpl implements PapelRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<Papel> findByUserId(int userId) {

		List<Papel> papeis;
		papeis = em.createQuery("SELECT u.papeis FROM Usuario u WHERE u.id = :userId")
				.setParameter("userId", userId).getResultList();
		
		return CollectionUtils.isEmpty(papeis ) ? null : papeis;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Papel> findByUserUsername(String username) {

		List<Papel> papeis;
		papeis = em.createQuery("SELECT u.papeis FROM Usuario u WHERE u.username = :username")
				.setParameter("username", username).getResultList();
		
		return CollectionUtils.isEmpty(papeis ) ? null : papeis;
	}
}