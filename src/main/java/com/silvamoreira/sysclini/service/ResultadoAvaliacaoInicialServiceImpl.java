package com.silvamoreira.sysclini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.silvamoreira.sysclini.dao.ResultadoAvaliacaoInicialRepository;
import com.silvamoreira.sysclini.entity.ResultadoAvaliacaoInicial;

@Service
public class ResultadoAvaliacaoInicialServiceImpl implements ResultadoAvaliacaoInicialService {

	@Autowired
	ResultadoAvaliacaoInicialRepository resultadoAvaliacaoInicialRepository;
	
	@Override
	public ResultadoAvaliacaoInicial findById(int id) {
		ResultadoAvaliacaoInicial resultadoAvaliacaoInicial = null;

		Optional<ResultadoAvaliacaoInicial> result = resultadoAvaliacaoInicialRepository.findById(id);

		if (result.isPresent()) {
			resultadoAvaliacaoInicial = result.get();
		}

		else {
			// we didn't find the employee
			throw new RuntimeException("ResultadoAvaliacaoInicialRepository não encontrado. Id - " + id);
		}

		return resultadoAvaliacaoInicial;
	}

	@Override
	public List<ResultadoAvaliacaoInicial> findAll() {
		return resultadoAvaliacaoInicialRepository.findAll();
	}

	//TODO implementar depois
//	@Override
//	public ResultadoAvaliacaoInicial findByPacienteId(int pacienteId) {
//		return resultadoAvaliacaoInicialRepository.findByPacienteId(pacienteId);
//	}

	@Override
	public void save(ResultadoAvaliacaoInicial resultadoAvaliacaoInicial) {
		resultadoAvaliacaoInicialRepository.save(resultadoAvaliacaoInicial);
	}

}
