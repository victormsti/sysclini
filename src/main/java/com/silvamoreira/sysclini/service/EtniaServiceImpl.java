package com.silvamoreira.sysclini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.silvamoreira.sysclini.dao.EmpresaRepositoryCustom;
import com.silvamoreira.sysclini.dao.EstadoRepository;
import com.silvamoreira.sysclini.dao.EtniaRepository;
import com.silvamoreira.sysclini.dao.EmpresaRepository;
import com.silvamoreira.sysclini.entity.Empresa;
import com.silvamoreira.sysclini.entity.Estado;
import com.silvamoreira.sysclini.entity.Etnia;

@Service
public class EtniaServiceImpl implements EtniaService {

	@Autowired
	private EtniaRepository etniaRespository;
	
	@Override
	public Etnia findById(int id) {

		Etnia etnia = null;

		Optional<Etnia> result = etniaRespository.findById(id);

		if (result.isPresent()) {
			etnia = result.get();
		}

		else {
			// we didn't find the employee
			throw new RuntimeException("Etnia não encontrada. Id - " + id);
		}

		return etnia;
	}

	@Override
	public List<Etnia> findAll() {
		
		return etniaRespository.findAll();
	}

}
