package com.silvamoreira.sysclini.dao;

import java.util.List;

import com.silvamoreira.sysclini.entity.Cidade;

public interface CidadeRepositoryCustom {

	List<Cidade> findByEstadoId(int estadoId);

}
