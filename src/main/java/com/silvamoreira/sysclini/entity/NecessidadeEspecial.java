package com.silvamoreira.sysclini.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="necessidade_especial", schema="comum")
public class NecessidadeEspecial {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_necessidade_especial", updatable = false, nullable = false)
	private int id;
	
	@Column(name = "baixa_visao", nullable = false)
	private Boolean baixaVisao;
	
	@Column(name = "cegueira", nullable = false)
	private Boolean cegueira;
	
	@Column(name = "deficiencia_auditiva", nullable = false)
	private Boolean deficienciaAuditiva;
	
	@Column(name = "deficiencia_fisica", nullable = false)
	private Boolean deficienciaFisica;
	
	@Column(name = "deficiencia_intelectual", nullable = false)
	private Boolean deficienciaIntelectual;
	
	@Column(name = "surdez", nullable = false)
	private Boolean surdez;
	
	@Column(name = "surdocegueira", nullable = false)
	private Boolean surdocegueira;
	
	@Column(name = "deficiencia_multipla", nullable = false)
	private Boolean deficienciaMultipla;
	
	@Column(name = "autismo_infantil", nullable = false)
	private Boolean autismoInfantil;
	
	@Column(name = "sindrome_de_asperger", nullable = false)
	private Boolean sindromeDeAsperger;
	
	@Column(name = "sindrome_de_rett", nullable = false)
	private Boolean sindromeDeRett;
	
	@Column(name = "transtorno_desintegrativo_infancia", nullable = false)
	private Boolean transtornoDesintegrativoInfancia;
	
	@Column(name = "altas_habilidades_superdotacao", nullable = false)
	private Boolean altasHabilidadesSuperdotacao;
	
	@Column(name = "auxilio_ledor", nullable = false)
	private Boolean auxilioLedor;
	
	@Column(name = "auxilio_transcricao", nullable = false)
	private Boolean auxilioTranscricao;
	
	@Column(name = "guia_interprete", nullable = false)
	private Boolean guiaInterprete;
	
	@Column(name = "interprete_libras", nullable = false)
	private Boolean interpreteLibras;
	
	@Column(name = "leitura_labial", nullable = false)
	private Boolean leituraLabial;
	
	@Column(name = "prova_ampliada_16", nullable = false)
	private Boolean provaAmpliada16;
	
	@Column(name = "prova_ampliada_20", nullable = false)
	private Boolean provaAmpliada20;
	
	@Column(name = "prova_ampliada_24", nullable = false)
	private Boolean provaAmpliada24;
	
	@Column(name = "prova_braille", nullable = false)
	private Boolean provaBraille;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Boolean getBaixaVisao() {
		return baixaVisao;
	}

	public void setBaixaVisao(Boolean baixaVisao) {
		this.baixaVisao = baixaVisao;
	}

	public Boolean getCegueira() {
		return cegueira;
	}

	public void setCegueira(Boolean cegueira) {
		this.cegueira = cegueira;
	}

	public Boolean getDeficienciaAuditiva() {
		return deficienciaAuditiva;
	}

	public void setDeficienciaAuditiva(Boolean deficienciaAuditiva) {
		this.deficienciaAuditiva = deficienciaAuditiva;
	}

	public Boolean getDeficienciaFisica() {
		return deficienciaFisica;
	}

	public void setDeficienciaFisica(Boolean deficienciaFisica) {
		this.deficienciaFisica = deficienciaFisica;
	}

	public Boolean getDeficienciaIntelectual() {
		return deficienciaIntelectual;
	}

	public void setDeficienciaIntelectual(Boolean deficienciaIntelectual) {
		this.deficienciaIntelectual = deficienciaIntelectual;
	}

	public Boolean getSurdez() {
		return surdez;
	}

	public void setSurdez(Boolean surdez) {
		this.surdez = surdez;
	}

	public Boolean getSurdocegueira() {
		return surdocegueira;
	}

	public void setSurdocegueira(Boolean surdocegueira) {
		this.surdocegueira = surdocegueira;
	}

	public Boolean getDeficienciaMultipla() {
		return deficienciaMultipla;
	}

	public void setDeficienciaMultipla(Boolean deficienciaMultipla) {
		this.deficienciaMultipla = deficienciaMultipla;
	}

	public Boolean getAutismoInfantil() {
		return autismoInfantil;
	}

	public void setAutismoInfantil(Boolean autismoInfantil) {
		this.autismoInfantil = autismoInfantil;
	}

	public Boolean getSindromeDeAsperger() {
		return sindromeDeAsperger;
	}

	public void setSindromeDeAsperger(Boolean sindromeDeAsperger) {
		this.sindromeDeAsperger = sindromeDeAsperger;
	}

	public Boolean getSindromeDeRett() {
		return sindromeDeRett;
	}

	public void setSindromeDeRett(Boolean sindromeDeRett) {
		this.sindromeDeRett = sindromeDeRett;
	}

	public Boolean getTranstornoDesintegrativoInfancia() {
		return transtornoDesintegrativoInfancia;
	}

	public void setTranstornoDesintegrativoInfancia(
			Boolean transtornoDesintegrativoInfancia) {
		this.transtornoDesintegrativoInfancia = transtornoDesintegrativoInfancia;
	}

	public Boolean getAltasHabilidadesSuperdotacao() {
		return altasHabilidadesSuperdotacao;
	}

	public void setAltasHabilidadesSuperdotacao(Boolean altasHabilidadesSuperdotacao) {
		this.altasHabilidadesSuperdotacao = altasHabilidadesSuperdotacao;
	}

	public Boolean getAuxilioLedor() {
		return auxilioLedor;
	}

	public void setAuxilioLedor(Boolean auxilioLedor) {
		this.auxilioLedor = auxilioLedor;
	}

	public Boolean getAuxilioTranscricao() {
		return auxilioTranscricao;
	}

	public void setAuxilioTranscricao(Boolean auxilioTranscricao) {
		this.auxilioTranscricao = auxilioTranscricao;
	}

	public Boolean getGuiaInterprete() {
		return guiaInterprete;
	}

	public void setGuiaInterprete(Boolean guiaInterprete) {
		this.guiaInterprete = guiaInterprete;
	}

	public Boolean getInterpreteLibras() {
		return interpreteLibras;
	}

	public void setInterpreteLibras(Boolean interpreteLibras) {
		this.interpreteLibras = interpreteLibras;
	}

	public Boolean getLeituraLabial() {
		return leituraLabial;
	}

	public void setLeituraLabial(Boolean leituraLabial) {
		this.leituraLabial = leituraLabial;
	}

	public Boolean getProvaAmpliada16() {
		return provaAmpliada16;
	}

	public void setProvaAmpliada16(Boolean provaAmpliada16) {
		this.provaAmpliada16 = provaAmpliada16;
	}

	public Boolean getProvaAmpliada20() {
		return provaAmpliada20;
	}

	public void setProvaAmpliada20(Boolean provaAmpliada20) {
		this.provaAmpliada20 = provaAmpliada20;
	}

	public Boolean getProvaAmpliada24() {
		return provaAmpliada24;
	}

	public void setProvaAmpliada24(Boolean provaAmpliada24) {
		this.provaAmpliada24 = provaAmpliada24;
	}

	public Boolean getProvaBraille() {
		return provaBraille;
	}

	public void setProvaBraille(Boolean provaBraille) {
		this.provaBraille = provaBraille;
	}
}
