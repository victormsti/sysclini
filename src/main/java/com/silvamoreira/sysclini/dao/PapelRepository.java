package com.silvamoreira.sysclini.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.silvamoreira.sysclini.entity.Papel;

public interface PapelRepository extends JpaRepository<Papel, Integer> {

	
}
