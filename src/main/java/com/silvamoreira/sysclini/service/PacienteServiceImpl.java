package com.silvamoreira.sysclini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.silvamoreira.sysclini.dao.PacienteRepository;
import com.silvamoreira.sysclini.entity.Estado;
import com.silvamoreira.sysclini.entity.Paciente;

@Service
public class PacienteServiceImpl implements PacienteService {

	@Autowired
	PacienteRepository pacienteRepository;

	@Override
	public Paciente findById(int id) {
		Paciente paciente = null;

		Optional<Paciente> result = pacienteRepository.findById(id);

		if (result.isPresent()) {
			paciente = result.get();
		}

		else {
			// we didn't find the employee
			throw new RuntimeException("Paciente não encontrado. Id - " + id);
		}

		return paciente;
	}

	@Override
	public List<Paciente> findAll() {
		return pacienteRepository.findAll();
	}

	@Override
	public void save(Paciente paciente) {

		pacienteRepository.save(paciente);
	}

}
