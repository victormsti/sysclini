package com.silvamoreira.sysclini.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="pessoa", schema="comum")
public class Pessoa {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_pessoa", updatable = false, nullable = false)
	private int id;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "cpf")
	private String cpf;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Column(name = "dataNascimento")
	private Date dataNascimento;
	
	@Column(name = "sexo")
	private char sexo;
	
	@Column(name = "nacionalidade")
	private String nacionalidade;
	
	@Column(name = "numeroIdentidade")
	private String numeroIdentidade;
	
	@Column(name = "ufIdentidade")
	private String ufIdentidade;
	
	@Column(name = "telefone")
	private String telefone;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "nome_arquivo")
	private String nomeArquivo;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_pais")
	private Pais pais;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_necessidade_especial")
	private NecessidadeEspecial necessidadeEspecial;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_etnia")
	private Etnia etnia;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_endereco")
	private Endereco endereco;
	
	@Transient
	private String dataFormatada;
	
	@Transient
	private String enderecoFoto;
	
	@Column(name = "extensao_arquivo")
	private String extensaoArquivo;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public String getNumeroIdentidade() {
		return numeroIdentidade;
	}

	public void setNumeroIdentidade(String numeroIdentidade) {
		this.numeroIdentidade = numeroIdentidade;
	}

	public String getUfIdentidade() {
		return ufIdentidade;
	}

	public void setUfIdentidade(String ufIdentidade) {
		this.ufIdentidade = ufIdentidade;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Etnia getEtnia() {
		return etnia;
	}

	public void setEtnia(Etnia etnia) {
		this.etnia = etnia;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public NecessidadeEspecial getNecessidadeEspecial() {
		return necessidadeEspecial;
	}

	public void setNecessidadeEspecial(NecessidadeEspecial necessidadeEspecial) {
		this.necessidadeEspecial = necessidadeEspecial;
	}

	public String getDataFormatada() {
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		return formato.format(dataNascimento);
	}

	public void setDataFormatada(String dataFormatada) {
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		this.dataFormatada = formato.format(dataNascimento);
	}

	public String getEnderecoFoto() {
		if (extensaoArquivo != null)
			return "/upload-dir/"+getId()+"."+getExtensaoArquivo().toLowerCase();
		else return null;
	}

	public void setEnderecoFoto(String enderecoFoto) {
		this.enderecoFoto = enderecoFoto;
	}

	public String getExtensaoArquivo() {
		return extensaoArquivo;
	}

	public void setExtensaoArquivo(String extensaoArquivo) {
		this.extensaoArquivo = extensaoArquivo;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
	
}
