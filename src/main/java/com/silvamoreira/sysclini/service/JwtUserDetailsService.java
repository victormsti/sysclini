package com.silvamoreira.sysclini.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.silvamoreira.sysclini.dao.UsuarioRepository;
import com.silvamoreira.sysclini.dao.UsuarioRepositoryImpl;
import com.silvamoreira.sysclini.entity.Usuario;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private UsuarioRepositoryImpl usuarioRepositoryImpl;

	@Autowired
	private PasswordEncoder bcryptEncoder;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuarioRepositoryImpl.findByUsername(username);
		if (usuario == null) {
			throw new UsernameNotFoundException("Usuário não encontrado com o username: " + username);
		}
		return new org.springframework.security.core.userdetails.User(usuario.getUsername(), usuario.getPassword(),
				new ArrayList<>());
	}
	
	//public UserDao save(UserDTO user) {
	public Usuario save(Usuario user) {
		Usuario novoUsuario = new Usuario();
		novoUsuario.setUsername(user.getUsername());
		novoUsuario.setPassword(bcryptEncoder.encode(user.getPassword()));
		return usuarioRepository.save(novoUsuario);
	}
}
