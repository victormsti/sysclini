package com.silvamoreira.sysclini.service;

import java.util.List;

import com.silvamoreira.sysclini.entity.ResultadoAvaliacaoInicial;

public interface ResultadoAvaliacaoInicialService {

	public ResultadoAvaliacaoInicial findById(int id);
	
	public List<ResultadoAvaliacaoInicial> findAll();
	
//	public ResultadoAvaliacaoInicial findByPacienteId(int pacienteId);
	
	public void save (ResultadoAvaliacaoInicial resultadoAvaliacaoInicial);
}
