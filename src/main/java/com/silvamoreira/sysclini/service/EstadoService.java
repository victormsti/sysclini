package com.silvamoreira.sysclini.service;

import java.util.List;

import com.silvamoreira.sysclini.entity.Estado;


public interface EstadoService {
	
	public Estado findById(int id);
	
	public List<Estado> findAll();
}
