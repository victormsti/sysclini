package com.silvamoreira.sysclini.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;

import com.silvamoreira.sysclini.entity.Usuario;

/**
 * 
 * @author Victor Matheus
 *
 */
@Controller
public class GenericController {

	public static boolean selecionaVinculoPaginaInicial = false;
	
	public static final String DIRETORIO_FOTOS = "/upload/";
	public static String operacaoCadastro = "operacaoCadastro";
	public static String operacaoAlteracao = "operacaoAlteracao";
	
	public static Usuario usuarioLogado = null;
	
	public static String nomeUsuario = null;
	
	public static int usuarioId = 0;

	public static List<String> errorList = new ArrayList<>();
	public static List<String> successList = new ArrayList<>();
	public static List<String> warningList = new ArrayList<>();
	
	public static String operacao = null;

	public static void init() {
		errorList.clear();
		successList.clear();
		warningList.clear();
	}

	public static void adicionarMensagemErro(String erro) {
		errorList.add(erro);
	}

	public static void adicionarMensagemSucesso(String sucess) {
		successList.add(sucess);
	}

	public static void adicionarMensagemAviso(String warning) {
		warningList.add(warning);
	}
	
	public static void setOperacao(String operacaoSelecionada) {
		operacao = operacaoSelecionada;
	}

	public boolean hasErrors() {
		if (!errorList.isEmpty()) {
			return true;
		} else
			return false;
	}

	public static Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public static void setUsuarioLogado(Usuario usuarioLogado) {
		GenericController.usuarioLogado = usuarioLogado;
	}

	public static String getNomeUsuario() {
		return nomeUsuario;
	}

	public static void setNomeUsuario(String nomeUsuario) {
		GenericController.nomeUsuario = nomeUsuario;
	}

	public static int getUsuarioId() {
		return usuarioId;
	}

	public static void setUsuarioId(int usuarioId) {
		GenericController.usuarioId = usuarioId;
	}
	
}
