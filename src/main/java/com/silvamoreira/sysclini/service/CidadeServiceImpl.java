package com.silvamoreira.sysclini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.silvamoreira.sysclini.dao.EmpresaRepositoryCustom;
import com.silvamoreira.sysclini.dao.CidadeRepository;
import com.silvamoreira.sysclini.dao.CidadeRepositoryCustom;
import com.silvamoreira.sysclini.entity.Cidade;
import com.silvamoreira.sysclini.entity.Empresa;

@Service
public class CidadeServiceImpl implements CidadeService {

	@Autowired
	private CidadeRepository cidadeRespository;
	
	@Autowired
	private CidadeRepositoryCustom cidadeRespositoryCustom;
	
	
	@Override
	public Cidade findById(int id) {
		
		Cidade cidade = null;
		
		Optional<Cidade> result = cidadeRespository.findById(id);

		if (result.isPresent()) {
			cidade = result.get();
		}

		else {
			// we didn't find the employee
			throw new RuntimeException("Cidade não encontrada. Id - " + id);
		}

		return cidade;
	}
	
	@Override
	public List<Cidade> findAll() {
		return cidadeRespository.findAll();
	}
	
	@Override
	public List<Cidade> findByEstadoId(int estadoId) {
		return cidadeRespositoryCustom.findByEstadoId(estadoId);
	}

	

}
