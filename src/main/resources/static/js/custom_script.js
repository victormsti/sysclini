// Datatables
$(document).ready(function() {
	$('#tabela').DataTable();
});

$('#inputDataNascimento').datepicker({
	  	format:"dd/mm/yyyy",
	    todayBtn: true,
	    clearBtn: true,
	    language: "pt-PT",
	    daysOfWeekDisabled: "0",
	    daysOfWeekHighlighted: "0"
	});

function goBack() {
//	  window.history.back();
//	  location.reload();
	
	
//	window.history.go(-1);
	location.href = document.referrer;
//	return false;
	}

$('.cpf').mask('000.000.000-00', {reverse: true});

$('.telefone').mask('00 00000-0000', {reverse: true});

//$('.data').mask('00/00/0000', {reverse: true});

tinymce.init({      
    selector: 'textarea',  // change this value according to your HTML
    auto_focus: 'element1',
    toolbar: 'undo redo | imageupload',
    setup: function(editor) {

            // create input and insert in the DOM
            var inp = $('<input id="tinymce-uploader" type="file" name="pic" accept="image/*" style="display:none">');
            $(editor.getElement()).parent().append(inp);

            // when a file is selected, upload it to the server
            inp.on("change", function(e){
              uploadFile($(this), editor);
            });


          function uploadFile(inp, editor) {
            var input = inp.get(0);
            var data = new FormData();
            data.append('files', input.files[0]);

            $.ajax({
              url: '${pageContext.request.contextPath}/a/images',
              type: 'POST',
              data: data,
              enctype: 'multipart/form-data',
              dataType : 'json',
              processData: false, // Don't process the files
              contentType: false, // Set content type to false as jQuery will tell the server its a query string request
              success: function(data, textStatus, jqXHR) {
                editor.insertContent('<img class="content-img" src="${pageContext.request.contextPath}' + data.location + '" data-mce-src="${pageContext.request.contextPath}' + data.location + '" />');
              },
              error: function(jqXHR, textStatus, errorThrown) {
                if(jqXHR.responseText) {
                  errors = JSON.parse(jqXHR.responseText).errors
                  alert('Error uploading image: ' + errors.join(", ") + '. Make sure the file is an image and has extension jpg/jpeg/png.');
                }
              }
            });
          }
    }
  });