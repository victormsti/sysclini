package com.silvamoreira.sysclini.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="empresa", schema="comum")
public class Empresa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_empresa", updatable = false, nullable = false)
	private int id;
	
	@Column(name="cnpj")
	private String cnpj;
	
	@Column(name="codigo")
	private String codigo;
	
	@Column(name="razaoSocial")
	private String razaoSocial;
	
	@Column(name="nomeGestor")
	private String nomeGestor;

	@Column(name="email")
	private String email;
	
	@Column(name="telefone")
	private String telefone;
	
	@NotEmpty(message="Este campo não pode ser nulo")
	@Column(name="titulo")
	private String titulo;
	
	@NotEmpty(message="Este campo não pode ser nulo")
	@Column(name="descricao")
	private String descricao;
	
	
//	@OneToOne(mappedBy="empresa", cascade= {CascadeType.DETACH, 
//			CascadeType.MERGE,
//			CascadeType.PERSIST,
//			CascadeType.REFRESH})
//	private Usuario usuario;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_endereco", nullable = false)
	private Endereco endereco;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNomeGestor() {
		return nomeGestor;
	}

	public void setNomeGestor(String nomeGestor) {
		this.nomeGestor = nomeGestor;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public String toString() {
		return "Empresa [id=" + id + ", cnpj=" + cnpj + ", codigo=" + codigo + ", razaoSocial=" + razaoSocial
				+ ", nomeGestor=" + nomeGestor + ", email=" + email + ", telefone=" + telefone + ", titulo=" + titulo
				+ ", descricao=" + descricao + ", endereco=" + endereco + "]";
	}

}
