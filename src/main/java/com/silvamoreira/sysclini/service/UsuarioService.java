package com.silvamoreira.sysclini.service;

import java.util.List;

import com.silvamoreira.sysclini.entity.Usuario;


public interface UsuarioService {

	public List<Usuario> findAll();
	
	public Usuario findById (int id);
	
	public void save (Usuario usuario);
	
	public void deleteById (int id);
	
	public Usuario findByUsername(String username);
	
	public Usuario findByEmail(String email);
}
