create schema comum;
create schema clinica;

drop table comum.estado cascade;
drop table comum.cidade cascade;
drop table comum.endereco cascade;
drop table comum.usuario cascade;
drop table comum.papel cascade;
drop table comum.empresa cascade;
drop table comum.usuario_papeis cascade;

drop table comum.pais cascade;
drop table comum.etnia cascade;
drop table comum.necessidade_especial cascade;
drop table comum.pessoa cascade;
drop table clinica.resultado_avaliacao_inicial cascade;
drop table clinica.programa_pedagogico cascade;
drop table clinica.paciente cascade;
drop table clinica.responsavel cascade;
drop table clinica.responsavel_paciente cascade;

create table comum.papel(
id_papel SERIAL NOT NULL PRIMARY KEY,
role varchar(100) NOT NULL,
titulo varchar(100) NOT NULL,
descricao varchar(250) NULL
);

create table comum.estado(
id_estado SERIAL NOT NULL PRIMARY KEY,
nome varchar(100) NOT NULL,
sigla varchar(20) NOT NULL
);

create table comum.cidade(
id_cidade SERIAL NOT NULL PRIMARY KEY,
nome varchar(200) NOT NULL,
id_estado INT NOT NULL,
FOREIGN KEY (id_estado) REFERENCES comum.estado (id_estado)
);

create table comum.endereco(
id_endereco SERIAL NOT NULL PRIMARY KEY,
id_cidade INT NOT NULL,
cep varchar(100) NULL,
logadouro varchar(200) NULL,
numero varchar(20) NULL,
bairro_povoado varchar(200) NULL,
complemento varchar(200) NULL,
FOREIGN KEY (id_cidade) REFERENCES comum.cidade (id_cidade)
);

create table comum.empresa(
id_empresa SERIAL NOT NULL PRIMARY KEY,
id_endereco INT NOT NULL,
cnpj varchar(100) NULL,
codigo varchar(50) NULL,
titulo varchar(100) NOT NULL,
descricao varchar(250) NOT NULL,
razao_social varchar(100) NULL,
nome_gestor varchar(100) NULL,
email varchar(100) NULL,
telefone varchar(100) NULL,
FOREIGN KEY (id_endereco) REFERENCES comum.endereco (id_endereco)
);

CREATE TABLE comum.usuario(
   id_usuario SERIAL NOT NULL PRIMARY KEY,
   id_empresa INT NOT NULL,
   username varchar(100) NOT NULL,
   email varchar(100) NULL,
   telefone varchar(100) NULL,
   password varchar(250) NOT NULL,
   enabled boolean NOT NULL DEFAULT FALSE,
   FOREIGN KEY (id_empresa) REFERENCES comum.empresa (id_empresa)
);
  
create table comum.usuario_papeis (
  id_usuario INT NOT NULL,
  id_papel INT NOT NULL,
  UNIQUE (id_usuario,id_papel),
  FOREIGN KEY (id_usuario) REFERENCES comum.usuario (id_usuario),
  FOREIGN KEY (id_papel) REFERENCES comum.papel (id_papel),
  PRIMARY KEY (id_usuario,id_papel)
);

create table comum.pais(
id_pais INT NOT NULL PRIMARY KEY,
nome VARCHAR(100) NOT NULL,
sigla VARCHAR(10) NOT NULL
);

create table comum.etnia(
id_etnia INT NOT NULL PRIMARY KEY,
descricao VARCHAR(100) NOT NULL
);

CREATE TABLE comum.necessidade_especial
(
   id_necessidade_especial SERIAL NOT NULL PRIMARY KEY,
   altas_habilidades_superdotacao bool NOT NULL,
   autismo_infantil bool NOT NULL,
   auxilio_ledor bool NOT NULL,
   auxilio_transcricao bool NOT NULL,
   baixa_visao bool NOT NULL,
   cegueira bool NOT NULL,
   deficiencia_auditiva bool NOT NULL,
   deficiencia_fisica bool NOT NULL,
   deficiencia_intelectual bool NOT NULL,
   deficiencia_multipla bool NOT NULL,
   guia_interprete bool NOT NULL,
   interprete_libras bool NOT NULL,
   leitura_labial bool NOT NULL,
   prova_ampliada_16 bool NOT NULL,
   prova_ampliada_20 bool NOT NULL,
   prova_ampliada_24 bool NOT NULL,
   prova_braille bool NOT NULL,
   sindrome_de_asperger bool NOT NULL,
   sindrome_de_rett bool NOT NULL,
   surdez bool NOT NULL,
   surdocegueira bool NOT NULL,
   transtorno_desintegrativo_infancia bool NOT NULL
);

create table clinica.programa_pedagogico(
id_programa_pedagogico SERIAL NOT NULL PRIMARY KEY,
nome VARCHAR(20) NULL
);

create table clinica.resultado_avaliacao_inicial(
id_resultado_avaliacao_inicial SERIAL NOT NULL PRIMARY KEY,
data_avaliacao TIMESTAMP NULL,
queixa_principal VARCHAR(4000) NULL,
historia VARCHAR(4000) NULL,
processo_avaliativo VARCHAR(4000) NULL,
observacoes_cognitivas_comportamentais VARCHAR(4000) NULL,
resultados_avaliativos VARCHAR(4000) NULL,
generalizacao_pensamento_abstrato_raciocinio VARCHAR(4000) NULL,
consideracoes_finais_orientacoes VARCHAR(4000) NULL,
encaminhamentos VARCHAR(2000) NULL
);


create table comum.pessoa(
id_pessoa SERIAL NOT NULL PRIMARY KEY,
id_pais INT NULL,
id_endereco INT NULL,
id_etnia INT NULL,
id_necessidade_especial INT NULL,
nome VARCHAR(200) NOT NULL,
cpf VARCHAR(50) NULL,
data_nascimento TIMESTAMP NULL,
sexo BPCHAR NOT NULL,
nacionalidade VARCHAR(100) NULL,
numero_identidade VARCHAR(50) NULL,
uf_identidade VARCHAR(10) NULL,
telefone VARCHAR(50) NULL,
nome_arquivo VARCHAR(100) NULL,
email VARCHAR(100) NULL,
extensao_arquivo VARCHAR(10) NULL,
FOREIGN KEY (id_pais) REFERENCES comum.pais (id_pais),
FOREIGN KEY (id_endereco) REFERENCES comum.endereco (id_endereco),
FOREIGN KEY (id_etnia) REFERENCES comum.etnia (id_etnia),
FOREIGN KEY (id_necessidade_especial) REFERENCES comum.necessidade_especial (id_necessidade_especial)
);

create table clinica.paciente(
id_paciente SERIAL NOT NULL PRIMARY KEY,
id_pessoa INT NOT NULL,
id_resultado_avaliacao_inicial INT NULL,
id_programa_pedagogico INT NULL,
FOREIGN KEY (id_pessoa) REFERENCES comum.pessoa (id_pessoa),
FOREIGN KEY (id_resultado_avaliacao_inicial) REFERENCES clinica.resultado_avaliacao_inicial (id_resultado_avaliacao_inicial),
FOREIGN KEY (id_programa_pedagogico) REFERENCES clinica.programa_pedagogico (id_programa_pedagogico)
);

create table clinica.responsavel(
id_responsavel SERIAL NOT NULL PRIMARY KEY,
id_pessoa INT NOT NULL
);

create table clinica.responsavel_paciente(
id_responsavel INT NOT NULL,
id_paciente INT NOT NULL,
UNIQUE (id_responsavel,id_paciente),
FOREIGN KEY (id_responsavel) REFERENCES clinica.responsavel (id_responsavel),
FOREIGN KEY (id_paciente) REFERENCES clinica.paciente (id_paciente),
PRIMARY KEY (id_responsavel,id_paciente)
);


INSERT INTO comum.papel (role, titulo, descricao) VALUES ('ROLE_ADMIN_SISTEMA', 'Administrador do Sistema', 'Papel de administrador do sistema');
INSERT INTO comum.papel (role, titulo, descricao) VALUES ('ROLE_ADMIN_EMPRESA', 'Administrador da Empresa', 'Papel de administrador da empresa');
INSERT INTO comum.papel (role, titulo, descricao) VALUES ('ROLE_FUNCIONARIO_EMPRESA', 'Funcionário da Empresa', 'Papel de funcionário da empresa');
INSERT INTO comum.papel (role, titulo, descricao) VALUES ('ROLE_SECRETARIO_EMPRESA', 'Secretário da Empresa', 'Papel de secretário da empresa');
INSERT INTO comum.papel (role, titulo, descricao) VALUES ('ROLE_USER', 'Usuário Comum', 'Usuário comum do sistema');

INSERT INTO comum.endereco (id_cidade,cep,logadouro,numero,bairro_povoado,complemento) VALUES (5354,'49042390','Rua X', '999','São Conrado', 'Conjunto Orlando Dantas');
INSERT INTO comum.endereco (id_cidade,cep,logadouro,numero,bairro_povoado,complemento) VALUES (5354,'49040700','Conjunto X', '888','Inácio Barbosa', 'Bloco X Apto X');

INSERT INTO comum.empresa (id_endereco,cnpj,codigo,titulo,descricao,razao_social,nome_gestor,email,telefone) VALUES (1,null,null,'Empresa Teste','Descrição da Empresa Teste','EMPRESA TESTE','TK',null,null);
INSERT INTO comum.empresa (id_endereco,cnpj,codigo,titulo,descricao,razao_social,nome_gestor,email,telefone) VALUES (2,null,null,'SilvaMoreira', 'Descrição da Empresa SilvaMoreira', 'SILVAMOREIRA', 'Victor Matheus', 'silvamoreira@gmail.com',null);

INSERT INTO comum.usuario(id_empresa,username,email,telefone,password,enabled) VALUES (1,'victorsilva','testevictor@teste.com','99999999999','$2a$10$ITaYEnWHIzmLVuX5VF0z7OOb48yBHn53y0280BmRwak304Hxg/cTq', true);
INSERT INTO comum.usuario(id_empresa,username,email,telefone,password,enabled) VALUES (1,'ludmila','testelud@teste.com','99999999988','$2a$10$ITaYEnWHIzmLVuX5VF0z7OOb48yBHn53y0280BmRwak304Hxg/cTq', true);
INSERT INTO comum.usuario(id_empresa,username,email,telefone,password,enabled) VALUES (1,'marley','testemarley@teste.com','99999999977','$2a$10$ITaYEnWHIzmLVuX5VF0z7OOb48yBHn53y0280BmRwak304Hxg/cTq', true);
INSERT INTO comum.usuario(id_empresa,username,email,telefone,password,enabled) VALUES (1,'migson','testemigson@teste.com','99999999955','$2a$10$ITaYEnWHIzmLVuX5VF0z7OOb48yBHn53y0280BmRwak304Hxg/cTq', true);




INSERT INTO comum.usuario_papeis (id_usuario, id_papel) VALUES (1, 1);
INSERT INTO comum.usuario_papeis (id_usuario, id_papel) VALUES (1, 5);
INSERT INTO comum.usuario_papeis (id_usuario, id_papel) VALUES (2, 5);
INSERT INTO comum.usuario_papeis (id_usuario, id_papel) VALUES (3, 4);
INSERT INTO comum.usuario_papeis (id_usuario, id_papel) VALUES (3, 5);
INSERT INTO comum.usuario_papeis (id_usuario, id_papel) VALUES (4, 2);
INSERT INTO comum.usuario_papeis (id_usuario, id_papel) VALUES (4, 3);




INSERT INTO comum.etnia (id_etnia,descricao) VALUES (1,'Branca');
INSERT INTO comum.etnia (id_etnia,descricao) VALUES (2,'Amarela');
INSERT INTO comum.etnia (id_etnia,descricao) VALUES (3,'Preta');
INSERT INTO comum.etnia (id_etnia,descricao) VALUES (4,'Indígena');
INSERT INTO comum.etnia (id_etnia,descricao) VALUES (5,'Parda');
INSERT INTO comum.etnia (id_etnia,descricao) VALUES (6,'Não declarada');

INSERT INTO comum.pais (id_pais,nome,sigla) VALUES (1,'Brasil','BRA');

INSERT INTO comum.pessoa (id_pais,id_endereco,id_etnia,id_necessidade_especial,nome,cpf,data_nascimento,sexo,nacionalidade,numero_identidade,uf_identidade,telefone,email)
VALUES (1,1,5,null,'Joazinho', '98765431234','2010-05-05 12:00:00.0','M','brasileira',null,null,null,null);

INSERT INTO comum.pessoa (id_pais,id_endereco,id_etnia,id_necessidade_especial,nome,cpf,data_nascimento,sexo,nacionalidade,numero_identidade,uf_identidade,telefone,email)
VALUES (1,2,4,null,'Mariazinha', '12345678987','2011-04-04 12:00:00.0','F','brasileira',null,null,null,null);

INSERT INTO comum.pessoa (id_pais,id_endereco,id_etnia,id_necessidade_especial,nome,cpf,data_nascimento,sexo,nacionalidade,numero_identidade,uf_identidade,telefone,email)
VALUES (1,1,5,null,'Pai de Joaozinho', '98989898989','1980-04-04 12:00:00.0','M','brasileira',null,null,'79988889889','teste@teste.com');

INSERT INTO comum.pessoa (id_pais,id_endereco,id_etnia,id_necessidade_especial,nome,cpf,data_nascimento,sexo,nacionalidade,numero_identidade,uf_identidade,telefone,email)
VALUES (1,2,4,null,'Mae de Mariazinha', '912211221','1982-04-04 12:00:00.0','F','brasileira',null,null,'7999776655','testemaria@teste.com');

INSERT INTO clinica.paciente (id_pessoa,id_resultado_avaliacao_inicial,id_programa_pedagogico) VALUES (1,null,null);
INSERT INTO clinica.paciente (id_pessoa,id_resultado_avaliacao_inicial,id_programa_pedagogico) VALUES (2,null,null);

INSERT INTO clinica.responsavel (id_pessoa) VALUES (3);
INSERT INTO clinica.responsavel (id_pessoa) VALUES (4);

INSERT INTO clinica.responsavel_paciente (id_responsavel,id_paciente) VALUES (1,1);
INSERT INTO clinica.responsavel_paciente (id_responsavel,id_paciente) VALUES (2,2);