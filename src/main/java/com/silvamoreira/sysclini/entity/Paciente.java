package com.silvamoreira.sysclini.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="paciente", schema="clinica")
public class Paciente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_paciente", updatable = false, nullable = false)
	private int id;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_pessoa", nullable = false)
	private Pessoa pessoa;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_programa_pedagogico")
	private ProgramaPedagogico programaPedagogico;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_resultado_avaliacao_inicial")
	private ResultadoAvaliacaoInicial resultadoAvaliacaoInicial;
	
	@ManyToMany(fetch=FetchType.LAZY,
			cascade= {CascadeType.PERSIST, CascadeType.MERGE,
			
	})
	@JoinTable(name="responsavel_paciente", schema="clinica",
	joinColumns=@JoinColumn(name="id_paciente"),
	inverseJoinColumns=@JoinColumn(name="id_responsavel")) 
	private List<Responsavel> responsaveis;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public ProgramaPedagogico getProgramaPedagogico() {
		return programaPedagogico;
	}

	public void setProgramaPedagogico(ProgramaPedagogico programaPedagogico) {
		this.programaPedagogico = programaPedagogico;
	}

	public ResultadoAvaliacaoInicial getResultadoAvaliacaoInicial() {
		return resultadoAvaliacaoInicial;
	}

	public void setResultadoAvaliacaoInicial(ResultadoAvaliacaoInicial resultadoAvaliacaoInicial) {
		this.resultadoAvaliacaoInicial = resultadoAvaliacaoInicial;
	}

	public List<Responsavel> getResponsaveis() {
		return responsaveis;
	}

	public void setResponsaveis(List<Responsavel> responsaveis) {
		this.responsaveis = responsaveis;
	}
	
}
