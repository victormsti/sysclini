package com.silvamoreira.sysclini.dao;

import com.silvamoreira.sysclini.entity.Usuario;

public interface UsuarioRepositoryCustom {

	Usuario findByUsername(String username);
	Usuario findByEmail(String email);
}
