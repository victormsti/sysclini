package com.silvamoreira.sysclini;

import javax.annotation.Resource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.silvamoreira.sysclini.service.StorageService;

@SpringBootApplication
//public class SyscliniApplication implements CommandLineRunner {
public class SyscliniApplication {

	@Resource
	StorageService storageService;

	public static void main(String[] args) {
		SpringApplication.run(SyscliniApplication.class, args);
	}

//	@Override
//	public void run(String... arg) throws Exception {
//		storageService.deleteAll();
//		storageService.init();
//	}
}
