package com.silvamoreira.sysclini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.silvamoreira.sysclini.dao.UsuarioRepository;
import com.silvamoreira.sysclini.dao.UsuarioRepositoryCustom;
import com.silvamoreira.sysclini.entity.Usuario;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	private UsuarioRepository usuarioRepository;
	private UsuarioRepositoryCustom usuarioRepositoryCustom;
	@Autowired
	public UsuarioServiceImpl(UsuarioRepository usuarioRepository, UsuarioRepositoryCustom usuarioRepositoryCustom) {
		this.usuarioRepository = usuarioRepository;
		this.usuarioRepositoryCustom = usuarioRepositoryCustom;
		
	}

	@Override
	public List<Usuario> findAll() {
		
		return usuarioRepository.findAll();
	}

	@Override
	public Usuario findById(int id) {
		Optional<Usuario> result = usuarioRepository.findById(id);

		Usuario usuario = null;

		if (result.isPresent()) {
			usuario = result.get();
		}

//		else {
//			// we didn't find the employee
//			throw new RuntimeException("Usuário não encontrado. Id - " + id);
//		}

		return usuario;
	}

	@Override
	public void save(Usuario usuario) {
		usuarioRepository.save(usuario);
	}

	@Override
	public void deleteById(int id) {
		usuarioRepository.deleteById(id);
	}

	@Override
	public Usuario findByUsername(String username) {
		Usuario usuario = null;
		usuario = usuarioRepositoryCustom.findByUsername(username);
		if(usuario != null) {
		return usuario;
		}
		else return null;
	}
	
	@Override
	public Usuario findByEmail(String email) {
		Usuario usuario = null;
		usuario = usuarioRepositoryCustom.findByEmail(email);
		if(usuario != null) {
		return usuario;
		}
		else return null;
	}

}
