package com.silvamoreira.sysclini.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.silvamoreira.sysclini.entity.Cidade;
import com.silvamoreira.sysclini.service.CidadeService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CidadeController {

	@Autowired
	private CidadeService cidadeService;
	
	@GetMapping("/cidades")
	public List<Cidade> getCidadesByEstadoId(@RequestParam(name = "estadoId", required = false) String estadoId) {
		return cidadeService.findByEstadoId(Integer.parseInt(estadoId));

	}
}