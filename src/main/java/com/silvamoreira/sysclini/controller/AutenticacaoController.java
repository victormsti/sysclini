package com.silvamoreira.sysclini.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.silvamoreira.sysclini.config.JwtTokenUtil;
import com.silvamoreira.sysclini.entity.JwtRequest;
import com.silvamoreira.sysclini.entity.JwtResponse;
import com.silvamoreira.sysclini.entity.Papel;
import com.silvamoreira.sysclini.entity.Usuario;
import com.silvamoreira.sysclini.service.JwtUserDetailsService;
import com.silvamoreira.sysclini.service.PapelService;
import com.silvamoreira.sysclini.service.UsuarioService;
import com.silvamoreira.utils.OperacaoUtils;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class AutenticacaoController extends GenericController{

	@Autowired
	UsuarioService usuarioService;

	@Autowired
	PapelService papelService;

	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;
	
	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getUsername());

		final String token = jwtTokenUtil.generateToken(userDetails);

		return ResponseEntity.ok(new JwtResponse(token));
	}

//	@GetMapping("/")
//	public String showHome(Model model) {
//		// Verifica se o usuário já fez a seleção inicial de vínculo
//		if (!selecionaVinculoPaginaInicial) {
//
//			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//
//			Usuario usuario = usuarioService.findByUsername(auth.getName());
//			List<Papel> papeis = usuario.getPapeis();
//
//			if (papeis.size() > 1) {
//
//				// Retira todas as permissões do sistema para selecionar um vínculo antes
//				List<GrantedAuthority> authorities = new ArrayList<>(auth.getAuthorities());
//
//				authorities.clear();
//				// Usuário temporário para seleção de vínculo
//				authorities.add(new SimpleGrantedAuthority("ROLE_TEMP_ROLE"));
//
//				Authentication newAuth = new UsernamePasswordAuthenticationToken(auth.getPrincipal(),
//						auth.getCredentials(), authorities);
//				SecurityContextHolder.getContext().setAuthentication(newAuth);
//				
//				model.addAttribute("tituloOperacao", OperacaoUtils.TITULO_SELECAO_VINCULO);
//				model.addAttribute("descricaoOperacao", OperacaoUtils.DESCRICAO_SELECAO_VINCULO);
//
//				model.addAttribute("papeis", papeis);
//
//				return "selecionar_vinculo";
//			} else {
//				return "home";
//			}
//		} else
//			return "home";
//	}

//	@RequestMapping(value = "/login")
//	public String login(HttpSession session) {
//		session.invalidate();
//		return "login";
//	}

	@RequestMapping("/access-denied")
	public String acessoNegado(Model model) {

		model.addAttribute("tituloOperacao", OperacaoUtils.TITULO_ACESSO_NEGADO);
		model.addAttribute("descricaoOperacao", OperacaoUtils.DESCRICAO_ACESSO_NEGADO);

		return "acesso_negado";
	}

	@RequestMapping("logout")
	public String logout(HttpSession session) {
		session.invalidate();
		selecionaVinculoPaginaInicial = false;
		setUsuarioLogado(null);
		return "redirect:login";
	}

	@GetMapping("home")
	public String selecionarVinculo(Model model, HttpServletResponse response) throws IOException {

		if (selecionaVinculoPaginaInicial)
			return "home";

		else {

			response.sendRedirect("/mostrarVinculos");
			return null;
		}
	}

	@PostMapping("home")
	public String selecaoVinculo(@RequestParam("papelId") String papelId, Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Papel papel = papelService.findById(Integer.parseInt(papelId));
		List<GrantedAuthority> authorities = new ArrayList<>(auth.getAuthorities());

		// Limpa a lista de papeis autorizados para autenticar novamente com apenas um
		// papel
		authorities.clear();
		authorities.add(new SimpleGrantedAuthority(papel.getRole()));

		Authentication newAuth = new UsernamePasswordAuthenticationToken(auth.getPrincipal(), auth.getCredentials(),
				authorities);

		SecurityContextHolder.getContext().setAuthentication(newAuth);

		selecionaVinculoPaginaInicial = true;
//		model.addAttribute("usuario", usuarioService.findByUsername(auth.getName()));
		
		setNomeUsuario(auth.getName());
		return "home";
	}

	@GetMapping("/mostrarVinculos")
	public String mostrarVinculos(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Usuario usuario = usuarioService.findByUsername(auth.getName());

		List<Papel> papeis = usuario.getPapeis();

		if (selecionaVinculoPaginaInicial) {
			model.addAttribute("tituloOperacao", OperacaoUtils.TITULO_MOSTRAR_VINCULO);
			model.addAttribute("descricaoOperacao", OperacaoUtils.DESCRICAO_MOSTRAR_VINCULO);
		}

		else {
			model.addAttribute("tituloOperacao", OperacaoUtils.TITULO_SELECAO_VINCULO);
			model.addAttribute("descricaoOperacao", OperacaoUtils.DESCRICAO_SELECAO_VINCULO);
		}
		model.addAttribute("papeis", papeis);
		return "selecionar_vinculo";
	}
}
