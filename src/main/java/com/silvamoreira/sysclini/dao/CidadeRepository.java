package com.silvamoreira.sysclini.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.silvamoreira.sysclini.entity.Cidade;

public interface CidadeRepository extends JpaRepository<Cidade, Integer> {

	
}
