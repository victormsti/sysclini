package com.silvamoreira.sysclini.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.silvamoreira.sysclini.entity.ResultadoAvaliacaoInicial;


public interface ResultadoAvaliacaoInicialRepository extends JpaRepository<ResultadoAvaliacaoInicial, Integer> {

}
