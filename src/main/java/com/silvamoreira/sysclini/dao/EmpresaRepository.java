package com.silvamoreira.sysclini.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.silvamoreira.sysclini.entity.Empresa;


public interface EmpresaRepository extends JpaRepository<Empresa, Integer> {

	
}
