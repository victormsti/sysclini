package com.silvamoreira.sysclini.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.silvamoreira.sysclini.entity.Empresa;

public class EmpresaRepositoryImpl implements EmpresaRepositoryCustom{

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public Empresa findByUserId(int userId) {

		Empresa empresa =  (Empresa) em.createQuery("SELECT e FROM Usuario u, Empresa e WHERE u.id_empresa = e.id_empresa AND u.id = :userId")
				.setParameter("userId", userId).setMaxResults(1).getSingleResult();
		
		return empresa;
	}
}
