package com.silvamoreira.sysclini.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.silvamoreira.sysclini.dao.PapelRepository;
import com.silvamoreira.sysclini.dao.PapelRepositoryCustom;
import com.silvamoreira.sysclini.entity.Cidade;
import com.silvamoreira.sysclini.entity.Papel;

@Service
public class PapelServiceImpl implements PapelService {

	@PersistenceContext
	private EntityManager em;
	
	private PapelRepository papelRespository;
	private PapelRepositoryCustom papelRespositoryCustom;
	
	@Autowired
	public PapelServiceImpl(PapelRepository papelRespository, PapelRepositoryCustom papelRespositoryCustom) {
	this.papelRespository = papelRespository;
	this.papelRespositoryCustom = papelRespositoryCustom;
	}

	@Override
	public Papel findById(int id) {

		Papel papel = null;

		Optional<Papel> result = papelRespository.findById(id);

		if (result.isPresent()) {
			papel = result.get();
		}

		else {
			// we didn't find the employee
			throw new RuntimeException("Papel não encontrado. Id - " + id);
		}

		return papel;
	}
	
	@Override
	public List<Papel> findByUsername(String username) {

		return papelRespositoryCustom.findByUserUsername(username);
		
	}

	@Override
	public List<Papel> findAll() {
		
		return papelRespository.findAll();
	}

	@Override
	public List<Papel> findByUserId(int userId) {
		return papelRespositoryCustom.findByUserId(userId);
	}

}
