package com.silvamoreira.sysclini.service;

import java.util.List;

import com.silvamoreira.sysclini.entity.Etnia;


public interface EtniaService {
	
	public Etnia findById(int id);
	
	public List<Etnia> findAll();
}
