package com.silvamoreira.sysclini.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.silvamoreira.sysclini.entity.Paciente;


public interface PacienteRepository extends JpaRepository<Paciente, Integer> {

	
}
