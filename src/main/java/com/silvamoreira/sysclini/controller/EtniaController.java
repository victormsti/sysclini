package com.silvamoreira.sysclini.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.silvamoreira.sysclini.entity.Etnia;
import com.silvamoreira.sysclini.service.EtniaService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class EtniaController {

	@Autowired
	EtniaService etniaService;
	
	@GetMapping("/etnias")
	public List<Etnia> getEtnias() {
		return etniaService.findAll();

	}
}
