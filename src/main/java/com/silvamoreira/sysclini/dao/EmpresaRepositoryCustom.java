package com.silvamoreira.sysclini.dao;

import com.silvamoreira.sysclini.entity.Empresa;

public interface EmpresaRepositoryCustom {

	Empresa findByUserId(int userId);
}
