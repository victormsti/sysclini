package com.silvamoreira.sysclini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.silvamoreira.sysclini.dao.EmpresaRepositoryCustom;
import com.silvamoreira.sysclini.dao.EmpresaRepository;
import com.silvamoreira.sysclini.entity.Empresa;

@Service
public class EmpresaServiceImpl implements EmpresaService {

	private EmpresaRepository empresaRespository;
	private EmpresaRepositoryCustom empresaRespositoryCustom;
	
	@Autowired
	public EmpresaServiceImpl(EmpresaRepository empresaRespository, EmpresaRepositoryCustom empresaRespositoryCustom) {
	
		this.empresaRespository = empresaRespository;
		this.empresaRespositoryCustom = empresaRespositoryCustom;
	}
	@Override
	public Empresa findById(int id) {

		Empresa empresa = null;

		Optional<Empresa> result = empresaRespository.findById(id);

		if (result.isPresent()) {
			empresa = result.get();
		}

		else {
			// we didn't find the employee
			throw new RuntimeException("Empresa não encontrada. Id - " + id);
		}

		return empresa;
	}

	@Override
	public List<Empresa> findAll() {
		
		return empresaRespository.findAll();
	}

	@Override
	public Empresa findByUserId(int userId) {
		Empresa empresa = null;
		
		empresa = empresaRespositoryCustom.findByUserId(userId);

		if(empresa != null) {
			return empresa;
			}
			else return null;
		}
	@Override
	public void save(Empresa empresa) {
		empresaRespository.save(empresa);
	}

}
