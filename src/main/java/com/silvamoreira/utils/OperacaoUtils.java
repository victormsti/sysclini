package com.silvamoreira.utils;

public class OperacaoUtils {

	public static final String TITULO_ACESSO_NEGADO = "Acesso Negado";
	public static final String DESCRICAO_ACESSO_NEGADO = "Você não tem permissão para acessar esta funcionalidade. Talvez seja necessário trocar de vínculo";
	
	public static final String TITULO_SELECAO_VINCULO = "Selecionar Vínculo";
	public static final String DESCRICAO_SELECAO_VINCULO = "Foram detectados mais de um vínculo ativo relacionados ao seu usuário. Por favor, selecione um deles para operar no sistema";
	
	public static final String TITULO_MOSTRAR_VINCULO = "Selecionar Vínculo";
	public static final String DESCRICAO_MOSTRAR_VINCULO = "Nesta funcionalidade é possível selecionar um vínculo para operar no sistema";
	
	public static final String TITULO_CADASTRAR_USUARIO = "Cadastrar Novo Usuário";
	public static final String DESCRICAO_CADASTRAR_USUARIO = "Nesta funcionalidade é possível cadastrar um novo usuário";
	
	public static final String TITULO_CADASTRAR_EMPRESA = "Cadastrar Nova Empresa";
	public static final String DESCRICAO_CADASTRAR_EMPRESA = "Nesta funcionalidade é possível cadastrar uma nova empresa";
	
	public static final String TITULO_ALTERAR_USUARIO = "Alterar Usuário";
	public static final String DESCRICAO_ALTERAR_USUARIO = "Nesta funcionalidade é possível alterar um usuário";
	
	public static final String TITULO_ALTERAR_EMPRESA = "Alterar Empresa";
	public static final String DESCRICAO_ALTERAR_EMPRESA = "Nesta funcionalidade é possível alterar uma empresa";
	
	public static final String TITULO_GERENCIAR_USUARIO = "Gerenciar Usuários";
	public static final String DESCRICAO_GERENCIAR_USUARIO = "Nesta funcionalidade é possível gerenciar os usuários	cadastrados no sistema e inserir novos";
	
	public static final String TITULO_GERENCIAR_EMPRESA = "Gerenciar Empresas";
	public static final String DESCRICAO_GERENCIAR_EMPRESA = "Nesta funcionalidade é possível gerenciar as empresas cadastradas no sistema e inserir novas";

	public static final String TITULO_GERENCIAR_PACIENTE = "Gerenciar Pacientes";
	public static final String DESCRICAO_GERENCIAR_PACIENTE = "Nesta funcionalidade é possível gerenciar os pacientes cadastrados no sistema e inserir novos";
	
	public static final String TITULO_CADASTRAR_PACIENTE = "Cadastrar Paciente";
	public static final String DESCRICAO_CADASTRAR_PACIENTE = "Nesta funcionalidade é possível cadastrar um novo paciente. O cadastro é realizado por etapas";
	
	public static final String TITULO_ALTERAR_PACIENTE = "Alterar Paciente";
	public static final String DESCRICAO_ALTERAR_PACIENTE = "Nesta funcionalidade é alterar um paciente já cadastrado no sistema";
	
	public static final String TITULO_GERENCIAR_RESULTADO_AVALIACAO_PACIENTE = "Gerenciar Resultado da Avaliação inicial";
	public static final String DESCRICAO_GERENCIAR_RESULTADO_AVALIACAO_PACIENTE = "Nesta funcionalidade é possível gerenciar a avaliação inicial dos pacientes cadastrados no sistema e inserir novas avaliações";
	
	public static final String TITULO_CADASTRAR_RESULTADO_AVALIACAO_PACIENTE = "Cadastrar Resultado da Avaliação inicial";
	public static final String DESCRICAO_CADASTRAR_RESULTADO_AVALIACAO_PACIENTE = "Nesta funcionalidade é possível cadastrar a avaliação inicial do paciente selecionado";
}
