package com.silvamoreira.sysclini.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="papel", schema="comum")
public class Papel {
	
	
	@JsonCreator
	public Papel (@JsonProperty("id") Integer id ) {
	    this.id = id;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_papel", updatable = false, nullable = false)
	private int id;
	
	@Column(name="role")
	private String role;
	
	@Column(name="titulo")
	private String titulo;
	
	@Column(name="descricao")
	private String descricao;
	
	@Transient
	private String icone;
	
	@Transient
	private boolean selecionado;
	
	public Papel() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Papel(int id, String role, String titulo, String descricao) {
		super();
		this.id = id;
		this.role = role;
		this.titulo = titulo;
		this.descricao = descricao;
	}
	
	public String getIcone() {
		String icone = null;
		if(getRole().equalsIgnoreCase("ROLE_ADMIN_SISTEMA"))
			icone = "fa fa-tools";
		
		if(getRole().equalsIgnoreCase("ROLE_ADMIN_EMPRESA"))
			icone = "fa fa-business-time";
		
		if(getRole().equalsIgnoreCase("ROLE_FUNCIONARIO_EMPRESA"))
			icone = "fa fa-people-carry";
		
		if(getRole().equalsIgnoreCase("ROLE_SECRETARIO_EMPRESA"))
			icone = "fa fa-clipboard-list";
		
		if(getRole().equalsIgnoreCase("ROLE_USER"))
			icone = "fa fa-users";
		
		return icone;
	}

	public void setIcone(String icone) {
		this.icone = icone;
	}

	public boolean isSelecionado() {
		return selecionado;
	}

	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}
	
}
