package com.silvamoreira.sysclini.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="resultado_avaliacao_inicial", schema="clinica")
public class ResultadoAvaliacaoInicial {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_resultado_avaliacao_inicial", updatable = false, nullable = false)
	private int id;
	
	@Column(name = "data_avaliacao")
	private Date dataAvaliacao;
	
	@Column(name = "queixa_principal")
	private String queixaPrincipal;
	
	@Column(name = "historia")
	private String historia;
	
	@Column(name = "processo_avaliativo")
	private String processoAvaliativo;
	
	@Column(name = "observacoes_cognitivas_comportamentais")
	private String observacoesCognitivasComportamentais;
	
	@Column(name = "resultados_avaliativos")
	private String resultadosAvaliativos;
	
	@Column(name = "generalizacao_pensamento_abstrato_raciocinio")
	private String generalizacaoPensamentoAbstratoRaciocinio;
	
	@Column(name = "consideracoes_finais_orientacoes")
	private String consideracoesFinaisOrientacoes;
	
	@Column(name = "encaminhamentos")
	private String encaminhamentos;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDataAvaliacao() {
		return dataAvaliacao;
	}

	public void setDataAvaliacao(Date dataAvaliacao) {
		this.dataAvaliacao = dataAvaliacao;
	}

	public String getQueixaPrincipal() {
		return queixaPrincipal;
	}

	public void setQueixaPrincipal(String queixaPrincipal) {
		this.queixaPrincipal = queixaPrincipal;
	}

	public String getHistoria() {
		return historia;
	}

	public void setHistoria(String historia) {
		this.historia = historia;
	}

	public String getProcessoAvaliativo() {
		return processoAvaliativo;
	}

	public void setProcessoAvaliativo(String processoAvaliativo) {
		this.processoAvaliativo = processoAvaliativo;
	}

	public String getObservacoesCognitivasComportamentais() {
		return observacoesCognitivasComportamentais;
	}

	public void setObservacoesCognitivasComportamentais(String observacoesCognitivasComportamentais) {
		this.observacoesCognitivasComportamentais = observacoesCognitivasComportamentais;
	}

	public String getResultadosAvaliativos() {
		return resultadosAvaliativos;
	}

	public void setResultadosAvaliativos(String resultadosAvaliativos) {
		this.resultadosAvaliativos = resultadosAvaliativos;
	}

	public String getGeneralizacaoPensamentoAbstratoRaciocinio() {
		return generalizacaoPensamentoAbstratoRaciocinio;
	}

	public void setGeneralizacaoPensamentoAbstratoRaciocinio(String generalizacaoPensamentoAbstratoRaciocinio) {
		this.generalizacaoPensamentoAbstratoRaciocinio = generalizacaoPensamentoAbstratoRaciocinio;
	}

	public String getConsideracoesFinaisOrientacoes() {
		return consideracoesFinaisOrientacoes;
	}

	public void setConsideracoesFinaisOrientacoes(String consideracoesFinaisOrientacoes) {
		this.consideracoesFinaisOrientacoes = consideracoesFinaisOrientacoes;
	}

	public String getEncaminhamentos() {
		return encaminhamentos;
	}

	public void setEncaminhamentos(String encaminhamentos) {
		this.encaminhamentos = encaminhamentos;
	}
	
	
}
