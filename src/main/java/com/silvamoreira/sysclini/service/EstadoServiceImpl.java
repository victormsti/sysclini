package com.silvamoreira.sysclini.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.silvamoreira.sysclini.dao.EmpresaRepositoryCustom;
import com.silvamoreira.sysclini.dao.EstadoRepository;
import com.silvamoreira.sysclini.dao.EmpresaRepository;
import com.silvamoreira.sysclini.entity.Empresa;
import com.silvamoreira.sysclini.entity.Estado;

@Service
public class EstadoServiceImpl implements EstadoService {

	@Autowired
	private EstadoRepository estadoRespository;
	
	@Override
	public Estado findById(int id) {

		Estado estado = null;

		Optional<Estado> result = estadoRespository.findById(id);

		if (result.isPresent()) {
			estado = result.get();
		}

		else {
			// we didn't find the employee
			throw new RuntimeException("Estado não encontrado. Id - " + id);
		}

		return estado;
	}

	@Override
	public List<Estado> findAll() {
		
		return estadoRespository.findAll();
	}

}
